// Random number
import randomNumber from "random-number";

export const generateRandomNumber = (lastComicNumber: number) => {
  const num = randomNumber.generator({
    min: 1,
    max: lastComicNumber,
    integer: true,
  });

  return num();
};
