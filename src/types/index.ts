export interface RandomComicType {
  alt: string;
  day: string;
  img: string;
  link: string;
  month: string;
  news: string;
  num: number;
  safe_title: string;
  title: string;
  transcript: string;
  year: string;
  upvoters: string[];
  downvoters: string[];
}

export interface SignUpDataType {
  firstName: string;
  lastName: string;
  email: string;
  password: string;
}

export interface SignInDataType {
  email: string;
  password: string;
}
