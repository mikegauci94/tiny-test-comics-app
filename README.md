# Tiny Test - Comic Strips App

## Technologies used

- React.js (For front-end)
- Typescript (For typesafe code on front-end)
- Firebase (For Authentication)
- Firebase Firestore (For Data storage)
- NodeJs and Express (For proxy server)
- XKCD API (For fetching comics)
- TailwindCSS (For styling)

## How to run project locally

- git clone project
- navigate to the front-end directory named comic-strips-app-master
- run npm install
- run npm start
- the back-end directory named server is being being used production Heroku App so there is no need to install node modules